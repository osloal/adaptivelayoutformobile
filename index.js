   //适配兼容
   (function (doc, win) {
    // var docEle = doc.documentElement;
    const dpr = Math.min(win.devicePixelRatio, 3),
        scale = 1 / dpr,
        resizeEvent = 'orientationchange' in window ? 'orientationchange' : 'resize';

    var recalCulate = function () {
        var docEle = document.documentElement,
            w = docEle.clientWidth,
            num = (w > 375 ? 375 : w) / 375;       // **此时的375就是你设计稿的尺寸
        docEle.style.fontSize = (num * 100).toFixed(1) + 'px';
    };
    recalCulate();
    if (!doc.addEventListener) return;
    win.addEventListener(resizeEvent, recalCulate, false);
})(document, window);


// 适配各种设备尺寸
/*设备的独立像素宽度 */
console.log( document.documentElement.clientWidth  * 100 / 375);
 
function adapter(){
    var fontSize = document.documentElement.clientWidth  * 100 / 375;
    /* 如何给 html 设置 font-size*/
    document.documentElement.style.fontSize = fontSize + 'px';
}

adapter();

window.onresize = adapter;